const WebSocket = require('ws');
const fs = require('fs');
const os = require('os');
const fetch = require('node-fetch');
const auth = require('./auth.js');
//const db = require('./db_actions.js');
const MongoClient = require("mongodb").MongoClient;
const { URLSearchParams } = require('url');
const voices = ['oksana', 'jane', 'omazh', 'zahar', 'ermil'];


const wss = new WebSocket.Server({ port: 3030 });
const url = "mongodb://localhost:27017/";
const mongoClient = new MongoClient(url);
let soundsRegexp = {}
let clients = [];

const soundDir = '/home/yourjest/crabravebot-backend/sounds/';
let soundBank = {};
function updateSoundBank(){
  fs.readdir(soundDir, (err, files)=>{
    if(err){
      return console.log('Unable to scan dir' + err);
    }
    files.forEach((file)=>{
      soundBank[file.slice(0, -4)] = fs.readFileSync(soundDir + file, (err, con)=>{
        if(err){
          console.log(err)
        }
      }); 
    })
  });
}
updateSoundBank();
let db;
let collection;

mongoClient.connect(function(err, client){
  db = client.db("twitchtools");
  collection = db.collection("soundRegexp");
  updateSounds()
});
function updateSounds(){
  collection.find({}).forEach(function(e){
    soundsRegexp[e.fileName] = {content : e.content, flags : e.flags}
  })
}
wss.on('connection', function connection(ws) {
  ws.on('message', function incoming(unparsed_data) {
    let data = JSON.parse(unparsed_data);
    switch (data.command_type) {
      case "init":{
        clients[data.whoami] = ws;
        console.log(data.whoami + " connected.");
        break;
      }
      case "update_loolter": {
        if(clients["bot"] !== undefined && data.whoami === "mainp"){
          let send_data = {command_type : "update_loolter", whoami: "server"};
          clients["bot"].send(JSON.stringify(send_data));
        }
        break;
      }
      case "tts_message":{
        if(clients["mainp"] !== undefined && data.whoami === "bot"){
          const params = new URLSearchParams();
          params.append('text', data.text);
          params.append('lang', 'ru-RU');
          params.append('speed', 1);
          params.append('voice', voices[Math.floor(Math.random() * voices.length)]);

          fetch("https://tts.api.cloud.yandex.net/speech/v1/tts:synthesize", { method: 'POST',
                                                                               headers: {'Authorization': 'Api-Key ' + auth.ya_key},
                                                                               body: params})
          .then(res => res.arrayBuffer()).then(res => {
              let voiced_payload = {command_type: "chat_sound", sound_buf : Array.from(new Uint8Array(res)), whoami: "server"};
              clients["mainp"].send(JSON.stringify(voiced_payload));
          })
        }
        break;
      }
      case "chat_sound":{
        if(clients["mainp"] !== undefined && data.whoami === "bot"){
          let sound_payload = {command_type: "chat_sound", sound_buf : Array.from(soundBank[data.sound_type]), whoami: "server"}
          clients["mainp"].send(JSON.stringify(sound_payload))
        }
      }
      case "reset_attempts":{
        if(clients["bot"] !== undefined && data.whoami === "mainp"){
          let send_data = {command_type: "reset_attempts", whoami : "server"}
          clients["bot"].send(JSON.stringify(send_data));
        }
      }
      case "get_sounds":{
        if(clients["bot"] !== undefined){
          let send_data = {command_type: "get_sounds", "sound_regexp" : soundsRegexp, whoami: "server"}
          clients["bot"].send(JSON.stringify(send_data));
        }
      }
      case "add_sound":{
        if(data.whoami === "mainp"){
          let regName = data.fileName.slice(0,-4);
          soundsRegexp ={...soundsRegexp, [regName] : {"content": data.content, "flags" : data.flags} };
          fs.writeFile(soundDir + data.fileName, Buffer.from(data.byteData), (err, data) => {
            if (err) console.log(err);
          })
          updateSoundBank();
          collection.insertOne({fileName: regName, content: data.content, flags: data.flags}, (err, res)=>{
            if(err) console.log(err)
            updateSounds()
          });
          let send_data = {command_type: "get_sounds", "sound_regexp" : soundsRegexp, whoami: "server"}
          clients["bot"].send(JSON.stringify(send_data));
        }
      }
      default:
    }
  });
});
