var auth = require('./auth.js');
var pool = auth.psql_pool;
module.exports = {
	upsert : upsert,
	getInfo : getInfo
}
function upsert(users){
	let ins_query = "INSERT INTO twitch.followers_stones (user_id, user_stones) values ";
	users.forEach(function(el){
		if(el.stones !== 0){
			ins_query += "(" + el.user_id + " , " + el.stones +"), "
		}
	});

	ins_query = ins_query.slice(0, -2) + " ON CONFLICT (user_id) DO UPDATE set user_stones = twitch.followers_stones.user_stones + EXCLUDED.user_stones";
	//console.log(ins_query);
	pool.query(ins_query, (err, res) =>{
	})
}
async function getInfo(users){
	let sel_query = "SELECT * FROM twitch.followers_stones where user_id in (";
	users.forEach((el) => {
		sel_query += el.user_id + " , "
	});
	sel_query = sel_query.slice(0, -2) + ")";
	let result = [];
	await pool.query(sel_query)
		.then(res => {
			for(el in res.rows) {
				result.push({user_id: res.rows[el].user_id, stones: res.rows[el].user_stones});
			}

	});
	return result;
}
